#!/usr/bin/env python
# coding: utf-8

# In[1]:

import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import (
    Output, Input, State
)

import plotly.graph_objects as go
import plotly.express as px

from app import app, server, layouts_dict, qa_dict


# create layout dict

# sidebar contents
sidebar_data = {
    "Problem Overview": "overview",
    "Data" : [
        "data",
        {
            "Exploration": "data-exploration",
            "Transformation" : "data-transformation"
        }
    ],
    "Metrics": "metrics",
    "Clustering": "clustering",
    "Neighborhood": "neighbor",
}

# reverse lookup table
path_to_title = {"/": "overview"}
for key, value in sidebar_data.items():
    if isinstance(value, str):
        path_to_title[value] = key
        
    if isinstance(value, list):
        for subval in value:
            if isinstance(subval, str):
                path_to_title[subval] = key
            
            elif isinstance(subval, dict):
                for key2, value2 in subval.items():
                    # join keys
                    joined_key = f"{key} {key2}"
                    path_to_title[value2] = joined_key
path_to_title = {f"/{k}":v for k,v in path_to_title.items()}


# In[4]:


options_data = {
    "Dataset": ["ONET", "NOC"],
    "Method"  : ["Euclidean", "Cosine", "Mahalanobis"]
}


# In[5]:


def update_section_head(string):
    output = [html.P("I'm invisible :D   Made by", className="invis", id="invis-pad-1")]
    output.append(html.P(string))
    output.append(html.P("I'm invisible :D   Jacky Jang", className="invis", id="invis-pad-2"))
    output.append(html.Hr(id="section-head--hr"))
    return output


# In[6]:


def generate_sidebar(sidebar_data):
    ul_bullets = []
    for key, target in sidebar_data.items():
        if isinstance(target, str):
            # target is href key:
            li = html.Li(dcc.Link(key, href=target, className="left-white"))
            ul_bullets.append(li)

        if isinstance(target, list):
            # implies theres a nested html list of bullets.
            for target_item in target:
                if isinstance(target_item, str):
                    li = html.Li(dcc.Link(key, href=target_item, className="left-white"))
                    ul_bullets.append(li)

                if isinstance(target_item, dict):
                    # nested html list
                    ul_to_append = html.Ul()
                    temp_bullets = []

                    for nested_key, nested_target in target_item.items():
                        temp_bullets.append(
                            html.Li(dcc.Link(nested_key, href=nested_target, className="left-white")))
                    ul_to_append.children = temp_bullets

                    ul_bullets.append(ul_to_append)
    return ul_bullets


# In[7]:


def generate_options(options_data):
    settings_children = []
    for button_name, buttons in options_data.items():
        div_id = f"options--{button_name.lower()}"
        div_element = html.Div(
            id=div_id,
            children=dcc.RadioItems(
                id=f"{div_id}-radio",
                options = [
                    {"label": f"{button}\n", "value": button.lower()}
                    for button in buttons
                ],
                value=buttons[0].lower(),
                labelStyle={"display": "flex"}
            )
        )
        settings_children.append(html.Span(button_name))
        settings_children.append(div_element)
        
    return settings_children

def format_qa_list(qa_list):
    """
    input: list of <a>
    """
    if not qa_list:
        return ""
    
    ul_data = [html.P("Quick Access")]
    ul_data.extend([html.P(anchor) for anchor in qa_list])
    
    return html.Div(ul_data, id="index--qa-ul")
    
# In[9]:


app.layout = html.Div(
    id="main-page",
    children=[
        dcc.Location(id='url', refresh=False),
        dcc.Store(id="trash"),
        html.Div(id="scroll"),
    html.Div(
        className="container row-flex",
        children=[
            html.Div(
                id="left-controller",
                className="left-side container col-flex inline",
                children=[
                    html.Div(
                        id="banner",
                        children = [
                            html.P('Measuring'), 
                            html.P('Occupational'), 
                            html.P('Similarity'),
                            html.Hr(id="banner--hr"),
                        ]
                    ),
                    html.Div(
                        id="sidebar",
                        children = [
                            html.Div(
                                id="bullets",
                                children=generate_sidebar(sidebar_data)
                            ),
                            html.Div(
                                id="options",
                                children=generate_options(options_data)
                            )
                        ]
                    )
                ]
            ),
            html.Div(
                className="right-side container col-flex",
                children=[
                    html.Div(
                        id="section-head",
                        children = [
                            html.Div(
                                update_section_head("Problem Overview"),
                                id="section-head-title"
                            ),
                            html.Button(
                                "Focus", 
                                id="focused-button",
                                title="Leaving focus might not work when a graph has expanded."
                            )
                        ]
                    ),
                    html.Div(
                        id="content",
                        children=html.Div()
                    )
                ]
            ),
            html.Div(
                className="container col-flex page-toc inline",
                id="toc",
                children = [
                    html.Div(id="index--qa-list")
                ]
            )
            
        ]
    )
    ]
)

@app.callback(
    [
        Output("left-controller", "className"),
        Output("toc", "className"),
        Output("invis-pad-1", "className"),
        Output("invis-pad-2", "className")
    ],
    Input("focused-button", "n_clicks"),
    [
        State("left-controller", "className"),
        State("toc", "className"),
        State("invis-pad-1", "className"),
    ],
    prevent_initial_call=True
)
def focus_mode(click, lc_class, toc_class, ipad1_class):
    """
    Since all 3 move together, could technically do it for 1 and apply to 3.
    Sanity check -> for each
    """
    if "inline" in lc_class:
        lc = lc_class.replace("inline", "hidden")
    else:
        lc = lc_class.replace("hidden", "inline")
        
    if "inline" in toc_class:
        toc = toc_class.replace("inline", "hidden")
    else:
        toc = toc_class.replace("hidden", "inline")
        
    if "shrink" in ipad1_class:
        ipad = ipad1_class.replace("shrink", "")
    else:
        ipad = ipad1_class + " shrink"
        
    return lc, toc, ipad, ipad
    
    
    

app.clientside_callback(
    """
    function(pname) {
        window.scrollTo({top: 0, behavior: 'smooth'});
        return []
    }
    """,
    Output("scroll", "children"),
    Input("url", "pathname")
)   
    
    
@app.callback(
    [
        Output("section-head-title", "children"),
        Output("content", "children"),
        Output("index--qa-list", "children")
    ],
    [
        Input("url", "pathname")
    ]
)

def navigate_page(pathname):
    # page not found
    if pathname not in path_to_title:
        if pathname == "/":  # reroute
            pathname = "/overview"
        else:
            return (
                update_section_head("Page not found"),
                html.Div(),
                None
            )
    # handle content display
    if pathname[1:] not in layouts_dict:
        shown_layout = html.Div("Tab not yet implemented")
    else:
        shown_layout = layouts_dict[pathname[1:]]
    
    section_head = update_section_head(
        path_to_title[pathname]
    )
    
    # handle quick access links
    qa_list = qa_dict[pathname[1:]].qa_list

    return (
        section_head, 
        html.Div([shown_layout], style={"width": "80%"}), 
        format_qa_list(qa_list)
    )


if __name__ == "__main__":
    app.run_server(debug=True)

