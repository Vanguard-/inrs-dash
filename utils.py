import dash_html_components as html
import dash_core_components as dcc

import pandas as pd
import numpy as np

from sklearn.preprocessing import MinMaxScaler
from scipy.spatial.distance import pdist, squareform


interpolated_plasma = [
    "#0D0887", "#20068F", "#330597",
    "#46039F", "#5502A2", "#6302A5",
    "#7201A8", "#8008A5", "#8E10A1", 
    "#9C179E", "#A72296", "#B22C8E",
    "#BD3786", "#C6427D", "#CF4C74", 
    "#D8576B", "#DF6263", "#E66E5B",
    "#ED7953", "#F2864B", "#F69242",
    "#FB9F3A", "#FCAD33", "#FCBC2D",
    "#FDCA26", "#F9DA24", "#F4E923",
    "#F0F921",
]

class QuickAccess:
    def __init__(self):
        self.qa_list = []
        
    def set_anchor(self, string, element_id):
        """
        Return anchor inplace, add page menu ref to list
        """
        self.qa_list.append(
            html.A(string, href=f"#{element_id}")
#             dcc.Link(string, href=f"#{element_id}")
        )
        
        return html.A(
            html.H3(string),
            id=element_id
        )

def discrete_background_color(df, n_bins=28, columns="all"):
    bounds = [i * (1.0/n_bins) for i in range(n_bins + 1)]
    if columns == "all":
        if "id" in df:
            df_numeric_columns = df.select_dtypes("number").drop(["id"], axis=1)
        else:
            df_numeric_columns = df.select_dtypes("number")
    else:
        df_numeric_columns = df[columns]
        
    df_max = df_numeric_columns.max().max()
    df_min = df_numeric_columns.min().min()
    
    ranges = [
        ((df_max - df_min) * i) + df_min
        for i in bounds
    ]
    
    styles = []

    for i in range(1, len(bounds)):
        min_bound = ranges[i-1]
        max_bound = ranges[i]
        
        bg = interpolated_plasma[i-1]
        color = "white" if i < len(bounds)/2. else "inherit"
        
        for column in df_numeric_columns:
            styles.append({
                "if": {
                    "filter_query": (
                        "{{{column}}} >= {min_bound}" +
                        (" && {{{column}}} < {max_bound}" if (i < len(bounds) - 1) else "")
                    ).format(column=column, min_bound=min_bound, max_bound=max_bound),
                    "column_id" : column
                },
                "backgroundColor": bg,
                "color": color
            })
    
    return styles

def pwise(data, method="euclidean", **kwargs):
    scaler = MinMaxScaler((0,1))
    return pd.DataFrame(
        squareform(
            scaler.fit_transform(
                pdist(data, method).reshape(-1,1)
            ).flatten()
        ),
        index=data.index, columns=data.index
    )

def normalize_df(data):
    scaler = MinMaxScaler((0,1))
    return pd.DataFrame(
            scaler.fit_transform(data.T),
            index=data.index, columns=data.index
    )