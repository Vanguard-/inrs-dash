#!/usr/bin/env python
import pandas as pd

import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

import plotly.io as pio
pio.templates.default = "plotly_white"

import dash
import dash_table
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import (
    Output, Input, State
)

from app import app
from utils import QuickAccess

qa = QuickAccess()

df_occ_dist = pd.read_csv("data/occ_dist.csv")
df_occ_dist["Major Group Code"] = df_occ_dist["Major Group Code"].astype(str)

skills_mapping = (
    pd.read_json("data/skills.json", typ="series")
    .to_frame()
    .reset_index()
    .rename(columns={"index": "ID", 0:"Skill Name"})
    .set_index("ID").T
)

df_skills_dist = pd.read_csv("data/skills_dist.csv")
df_skills_dist["subset"] = df_skills_dist["Element ID"].str[4]

df_onet_codes = pd.read_csv("data/onet_codes.csv")



layout = html.Div([
    qa.set_anchor("Distribution of Occupations", "data-exploration--occ-dist"),
    dcc.Markdown(
        """
        There are 968 occupations split in 22 major groups. The 23rd group, 55 - Military Specific Operations, 
        does not include data-level occupations. 
        
        Note: Hover over the bars to see the major group's name in the tooltip.
        """,
        className="text"
    ),
    dcc.Graph(
        figure = px.bar(df_occ_dist, x="Major Group Code", y="Count", hover_data=["Major Group"])
    ),
    dcc.Markdown(
        """
        There is clear class imbalance, and the major group codes are somewhat sequentially related. 
        We would therefore expect very few connected components (up to 3),
        and depending on how well the skill vectors represent the reality of the occupations, there may be even less clusters.
        """,
        className="text"
    ),
    qa.set_anchor("Distribution of Skills", "data-eploration-skills-dist"),
    dcc.Markdown(
        """
        Each occupation is rated over 41 skills, each judged by their importance (IM) and level(LV).
        
        The graph below shows the distribution of both qualities for the subset of data displayed on the table. The graphs will update based on your filters.
        
        NOTE: There are issues with exiting "Focus" mode where the graph does not shrink back. Either refresh the page or change tabs to reset the state.
        
        """,
        className="text"
    ),
    # table and graps
    html.Div(
        children=[
            html.Div(
                dash_table.DataTable(
                    id = "data-exploration--skills-dist-table",
                    data=df_onet_codes.to_dict("records"),
                    columns = [{"name": i, "id": i} for i in df_onet_codes.columns],

                    page_action="none",
                    filter_action="native",
                    sort_action="native",

                    fixed_rows={'headers': True},
                    style_table = {"height": "500px", "overflowY": "auto"},  # adjust with subplot
                    style_cell={
                        'textAlign': 'left',
                        'whiteSpace': 'normal'
                    },
                    style_data_conditional=[
                        {'if': {'column_id': 'Code'},
                            'width': '110px'},
                        {'if': {'column_id': 'Title'},
                        'width': '190px'},
                    ],
                ),
                style = {"width": "325px"}
            ),
            html.Div(
                "testing",
                id="data-exploration--skills-subplots"
            ),
        ],
        id="data-exploration--skills-container",
        className = "container row-flex"
    ),
    # horizontal skills map
    html.Div(
        dash_table.DataTable(
            id="data-exploration--skills-mapping-table",
            data = skills_mapping.to_dict("records"),
            columns=[
                {'name': i, 'id': i} 
                for i in skills_mapping.columns
            ],
            style_table={
                'overflowX': 'auto',
                "position": "absolute",
            },
            style_cell={
                'height': 'auto',
                # all three widths are needed
                'minWidth': '200px', 'width': '200px', 'maxWidth': '200px',
                'whiteSpace': 'normal'
            }
        ),
        id="data-exploration--skills-mapping"
    ),
    dcc.Markdown(
        """
        Importance has range \[1,5\], and Level has range \[0,7\].        
        The key insights we draw from this lie in the symmetry within and between the skills.
        
        - For most the skills, the score distributions for level and importance are similar;
        - Most skills follow a similar distribution.
            
        If we did not have symmetry between the scales, then further analysis would require working with both vectors, 
        since both scales would provide novel information. Since that is not the case, we can follow Ormiston (2014)
        and multiply the scales together, producing a single 41-dimensional vector.
        
        Next, the symmetry between skills suggests that the skills do not represent a fully orthogonal space, 
        and that dimensionality reduction techniques such as Principal Component Analysis are viable in this domain.
        """,
        className="text"
    )
])

@app.callback(
    Output("data-exploration--skills-subplots", "children"),
    Input("data-exploration--skills-dist-table", "derived_virtual_data"),
)
def update_skills_dist_graph(rows):
    code_subset = pd.DataFrame(rows)
    target = code_subset["Code"].tolist()
    
    subsets = df_skills_dist.subset.unique()
    fig = make_subplots(
        4,1,
    )
    
    for subset in subsets:
        for side, scale, color in {("negative", "IM", "#EF553B"), ("positive","LV", "#636EFA")}:
            mask = (df_skills_dist["Scale ID"] == scale) & (df_skills_dist["subset"] == subset) & (df_skills_dist["Code"].isin(code_subset["Code"]))
            fig.add_trace(
                go.Violin(
                    x=df_skills_dist["Element ID"][mask], y=df_skills_dist["Data Value"][mask],
                    legendgroup=scale, scalegroup=scale, name=scale,
                    side = side, line_color=color
                ),row=int(subset), col=1
            )

    fig.update_layout(
        height=700,
#         paper_bgcolor='rgba(0,0,0,0)',
#         plot_bgcolor='rgba(0,0,0,0)'
    )
    fig.update_traces(meanline_visible=True)
    
    return html.Div(
        children = [
            dcc.Graph(figure=fig),
        ]
    )
    
