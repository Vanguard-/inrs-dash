#!/usr/bin/env python
import dash
import dash_table
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import (
    Output, Input, State,
    MATCH, ALL
)

from app import app
from utils import QuickAccess
qa = QuickAccess()

import pandas as pd

wa = pd.read_excel("data/wa.xlsx", engine="openpyxl", nrows=1000)
crosswalk = pd.read_csv("data/cleaned_crosswalk.csv")
skills_mapping = pd.read_json("data/skills.json", typ="series").to_frame().reset_index().rename(columns={"index": "ID", 0:"Skill Name"})



layout = html.Div([
    dcc.Markdown(
        """
        We use version 25.0 of the O*NET Work Activities dataset. The archive is accessible 
        [here](https://www.onetcenter.org/db_releases.html).
        
        There are 968 unique occupations, and 41 skills rated by their importance (IM) and level (LV). Applying the crosswalk
        to NOC codes results in 489 occupations. Below you can preview the (column-truncated) O\*NET Work Activities dataset, 
        the crosswalk, and the skills mapping.
        
        The O\*NET-SOC occupations are 8 digit codes, formatted as: MM-ABBC.DD, where

        - MM refers to a **major group**;
        - A refers to a **minor group**;
        - BB refers to the **broad occupation**;
        - C refers to the **detailed occupation**;
        - DD further subclasses the occupation.
        
        For more information, refer to the [tree layout](https://www.onetcenter.org/taxonomy/2019/structure.html) of the above,
        the [complete breakdown](https://www.onetcenter.org/taxonomy.html) by O\*NET, and 
        the Bureau of Labor Statistic's [official manual](https://www.bls.gov/soc/2018/soc_2018_manual.pdf).
        
        Note that the NOC field under Crosswalk is treated as numeric. Operators such as =, <, > are required when filtering.
        For performance purposes, only 1,000 rows are loaded in the O\*NET Work Activities dataset.
        """,
        className="text"
    ),
    html.Div(
        children=[
            dbc.Button(
                "O*NET",
                id={"tab": "data", "eltype": "btn", "element": "onet"}
            ),
            dbc.Button(
                "Skills Mapping",
                id={"tab": "data", "eltype": "btn", "element": "skills-mapping"}
            ),
            dbc.Button(
                "Crosswalk",
                id={"tab": "data", "eltype": "btn", "element": "crosswalk"}
            )
        ],
        id="data--buttons",
        className="row-flex"
    ),
    html.Div(
        children=[
            html.Div(
                children=[
                    dash_table.DataTable(
                        data=wa.to_dict('records'),
                        columns=[
                            {'name': i, 'id': i} 
                            for i in wa.columns
                        ],
                        fixed_rows={ 'headers': True, 'data': 0 },
                        sort_action="native",
                        style_cell={
                            'textAlign': 'left',
                            'whiteSpace': 'normal',
                        },
                        filter_action="native",
                        style_data_conditional=[
                            {'if': {'column_id': i},
                             'width': '50px'} 
                            for i in wa.columns
                        ],
                        page_action="native",
                        page_current= 0,
                        page_size= 41,
                        id="data--table-onet",
                    ),
                    html.Br(), html.Br()
                ],
                id="data--div-onet",
                style={"display": "hidden"}
            ),
            html.Div(
                children=[
                    dash_table.DataTable(
                        data=crosswalk.to_dict('records'),
                        columns=[
                            {'name': i, 'id': i} 
                            for i in crosswalk.columns
                        ],
                        fixed_rows={ 'headers': True, 'data': 0 },
                        sort_action="native",
                        style_cell={
                            'textAlign': 'left',
                            'whiteSpace': 'normal'
                        },
                        filter_action="native",
                        
                        style_data_conditional=[
                            {'if': {'column_id': 'NOC'},
                             'width': '50px'},
                            {'if': {'column_id': 'NOC Title'},
                             'width': '50px'},
                            {'if': {'column_id': 'O*NET'},
                             'width': '50px'},
                            {'if': {'column_id': 'O*NET Title'},
                             'width': '50px'},
                        ],
                        page_action="native",
                        page_current= 0,
                        id="data--table-crosswalk"
                    ),
                    html.Br(), html.Br()
                ],
                id="data--div-crosswalk",
                style={"display": "hidden"}
            ),
            html.Div(
                children=[
                    dash_table.DataTable(
                        data=skills_mapping.to_dict('records'),
                        columns=[
                            {'name': i, 'id': i} 
                            for i in skills_mapping.columns
                        ],
                        fixed_rows={ 'headers': True, 'data': 0 },
                        sort_action="native",
                        style_cell={
                            'textAlign': 'left',
                            'whiteSpace': 'normal'
                        },
                        filter_action="native",
                        
                        style_data_conditional=[
                            {'if': {'column_id': 'ID'},
                             'width': '50px'},
                            {'if': {'column_id': 'Skill Name'},
                             'width': '100px'},
                        ],
                        page_action="native",
                        page_current= 0,
                        id="data--table-skills-mapping"
                    ),
                    html.Br(), html.Br()
                ],
                id="data--div-skills-mapping",
                style={"display": "hidden"}
            )
        ]
    ),
])

@app.callback(
    [
        Output("data--div-onet", "style"),
        Output({"tab": "data", "eltype": "btn", "element": "onet"}, "active"),
        
        Output("data--div-crosswalk", "style"),
        Output({"tab": "data", "eltype": "btn", "element": "crosswalk"}, "active"),

        Output("data--div-skills-mapping", "style"),
        Output({"tab": "data", "eltype": "btn", "element": "skills-mapping"}, "active")
    ],
    [
        Input({"tab": "data", "eltype": "btn", "element": ALL}, "n_clicks")
    ],
#     prevent_initial_call=True
)
def data_toggle_datatable(btn):
    elements = {"onet", "crosswalk", "skills-mapping"}
    btn_to_div = {
        element: {"display": "none"} 
        for element in elements
    }
    
    active = {
        element: False
        for element in elements
    }
    

    ctx = dash.callback_context
    if not ctx.triggered:
        return {"display": "none"}, False, {"display": "none"}, False, {"display": "none"}, False
    else:
        last_clicked = ctx.triggered[0]["prop_id"].split(".")[0] # stringified dict
    
    for element in elements:
        if element in last_clicked:
            btn_to_div[element] = {"display": "inline"}
            active[element] = True
    
    return (
        btn_to_div["onet"], active["onet"],
        btn_to_div["crosswalk"], active["crosswalk"],
        btn_to_div["skills-mapping"], active["skills-mapping"]
    )
