import pandas as pd
import json
from plotly.subplots import make_subplots
import plotly.express as px
from scipy.cluster.hierarchy import dendrogram, linkage

from scipy.cluster.hierarchy import ClusterWarning
from warnings import simplefilter
simplefilter("ignore", ClusterWarning)

import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import (
    Output, Input, State
)

from app import app
from utils import QuickAccess, pwise, normalize_df
qa = QuickAccess()

with open("data/index/onet.json") as f:
    onet_map = json.load(f)
with open("data/index/noc.json") as f:
    noc_map = json.load(f)
    
df_onet_pca = pd.read_csv("data/onet_pca.csv", index_col="O*NET-SOC Code")
df_noc_pca = pd.read_csv("data/noc_pca.csv", index_col="NOC")
df_noc_pca.index = df_noc_pca.index.astype(str)



layout = html.Div([
    qa.set_anchor("Clustermaps", "clustering--clustermap"),
    dcc.Markdown(
        """
        Applying each of the distance functions pairwise lets us generate heatmaps to visually estimate the number of clusters and their
        respective sizes. We use dendrograms for hierarchical clustering, therefore generating a set of indices driven
        by the data instead of experts. Since each method measures distance using their respective scales, we normalize each column
        to a \[0, 1\] scale for enhanced cluster saturations.
        
        The clustermaps below are synchronized, and allow for a specific linkage method. 
        Re-indexing is done based on the clusters of the top-most clustermap, chosen in the sidebar.
        
        """,
        className="text"
    ),
    html.Div(
        children=[
            dcc.Dropdown(
                id="metrics--linkage-ddown",
                options = [
                    {"label": "Single"  , "value": "single"},
                    {"label": "Complete", "value": "complete"},
                    {"label": "Average" , "value": "average"},
                    {"label": "Centroid", "value": "centroid"},
                ],
                value="complete",
                className="text",
            ),
            dcc.Loading(
                dcc.Graph(id="metrics--clustermaps-graph"),
                parent_className='loading_wrapper'
            )
        ],
    ),
    dcc.Markdown(
        """
        While Mahalanobis distance produces the "noisiest" heatmap, we can see that all 3 functions generate
        similar clusters. Treating the heatmaps as weighted adjacency matrices, connected components are 
        represented by block matrices. Blocks along the diagonal represent the connection between occupations
        of the same cluster, and off-diagonal blocks estimate the relationship between occupations from different 
        occupations (e.g. transitions within and between industries).
        """,
        className="text"
    ),
#     qa.set_anchor("Dendrogram Analysis", "clustering--dendrograms"),
#     dcc.Markdown(
#         """
        
#         """
#     )
])


@app.callback(
    Output("metrics--clustermaps-graph", "figure"),
    [
        Input("options--dataset-radio", "value"),
        Input("metrics--linkage-ddown", "value"),
        Input("options--method-radio", "value")
    ]
)
def dist_plots(dataset, linkage_method, reference_index):
    
    if dataset == "onet":
        data = df_onet_pca
        mapping = onet_map
    else:
        data = df_noc_pca
        mapping = noc_map
    
    _METHODS = ["euclidean", "mahalanobis", "cosine"]
    
    METHODS = [reference_index]
    METHODS.extend([i for i in _METHODS if i != reference_index])
    
    big_fig = make_subplots(
        rows=len(METHODS),
        cols=1,
        shared_xaxes=True,
        subplot_titles = METHODS,
        vertical_spacing = 0.1
    )

    for idx, method in enumerate(METHODS):
        data_traces=[]
        piv1 = pwise(data, method)

        if idx == 0:
            benchmark_indices = dendrogram(
                linkage(piv1, method=linkage_method), no_plot=True
            )["leaves"]

        piv1 = piv1.iloc[benchmark_indices, benchmark_indices]
        
        piv1.index = piv1.index.map(mapping)
        piv1.columns = piv1.columns.map(mapping)

        piv1.index.name = "X"
        piv1.columns.name = "Y"

        piv1 = normalize_df(piv1)
        
        fig = px.imshow(
            piv1, 
            title=method,
        )


        for trace in range(len(fig["data"])):
            data_traces.append(fig["data"][trace])

        for trace in data_traces:
            big_fig.append_trace(trace, idx+1 ,1)
        
        big_fig.update_yaxes(visible=False, showticklabels=False)
        big_fig.update_xaxes(visible=False, showticklabels=False)
        
    return big_fig.update_layout(height=1000)
