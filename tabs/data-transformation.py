#!/usr/bin/env python
import pandas as pd
import pickle as pkl

from plotly.subplots import make_subplots

import dash
import dash_table
from dash_table.Format import Format, Scheme
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
from dash.dependencies import (
    Output, Input, State, ALL
)

from app import app
from utils import QuickAccess, discrete_background_color
qa = QuickAccess()

df_onet_piv = pd.read_csv("data/onet_piv.csv")
df_noc_piv = pd.read_csv("data/noc_piv.csv")

df_onet_pca = pd.read_csv("data/onet_pca.csv")
df_noc_pca = pd.read_csv("data/noc_pca.csv")

onet_loadings = pd.read_csv("data/transform/onet_loadings.csv")
noc_loadings = pd.read_csv("data/transform/noc_loadings.csv")

with open("data/transform/onet_pca_dict.pkl", "rb") as f:
    onet_pca_data = pkl.load(f)
    
with open("data/transform/noc_pca_dict.pkl", "rb") as f:
    noc_pca_data = pkl.load(f)

layout = html.Div([
    html.Div(
        children = [
            dcc.Markdown(
                """
                This section addresses how we handle the symmetry within and between skills. 
                Addressing internal skill symmetry reduced the number of attributes defining 
                an occupation from 82 to 41, and the symmetry between skills further reduced 
                the number of fields to 6 principal components preserving 80% variance (worthy 
                mention is that 4 skills preserve 75% variance). We finally created the NOC 
                dataset via a left join on the O\*NET dataset with the crosswalk.
                """,
                className="text"
            ),
            qa.set_anchor("Data Simplification: Level-Importance Symmetry", "data-transformation--symmetry-within"),
            dcc.Markdown(
                """
                Since the distribution for the level and importance pair is similar for most 
                skills, we can simplify the dataset by multiplying level (LV &isin; \[0,5\]<sup>41</sup>) 
                and importance (IM &isin; \[0,7\]<sup>41</sup>). The resulting variable is a 
                \[0, 35\]<sup>41</sup> vector, and our dataset is reduced from 82 variables to 41.
                """,
                dangerously_allow_html=True,
                className="text"
            ),
            
            qa.set_anchor("Dimension Reduction: Symmetry Between Skill", "data-transformation--symmetry-between"),
            dcc.Markdown(
                """
                Seeing that many of the 41 skills showed similar distributions, 
                we can try dimensionality reduction to reduce the underlying overweighting 
                of certain "broad" types of skills. The idea being that if similarly distributed 
                skills are more likely to represent the same broad skill, then we would be favoring 
                skills that are repeated more often under slight nuances. Broad types of skills 
                identified by past literature include categories such as "analytical", "technical",
                "physical", and "social".
                
                One such technique is Principal Component Analysis (PCA), which helps generate a set
                of linearly independent basis vectors that maximize the variance in the dataset.
                The idea is then to preserve a subset of these vectors based on their contribution 
                to the total variance. Since variance is scale-dependent, normalization is done to avoid 
                overweighting certain variables (e.g. the same measurement using meters and 
                millimeters as units will have vastly different variances). This is not an issue in 
                our dataset since every skill lies in the same range. Nevertheless, we normalized 
                the skills to have a range of \[0,1\], which is more interpretable than a number
                out of 35.
                """,
                className="text"
            ),
            dcc.Graph(id="data-transformation--pca-subplots"),
            dcc.Markdown(
                """
                The scree plot supports our hypothesis that many of the skills are redundant and 
                reflect a similar broad group of abilities. Moreover, the point of inflection also 
                supports the estimate that there are 4 broad groups. For our analysis, we will be 
                using n=6 components, preserving around 80% of total variance. The 2D and 3D PCA plots
                also display a very clear gradient on the clusters. For compatibility with the 
                colorscale, we removed the hyphen delimiting major and minor groups from O\*NET 
                codes. 
                
                Next, we analyze the eigenvectors obtained from the PCA. This is relevant because 
                the eigenvectors define the mapping from our previous frame of reference 
                (occupations defined by 41 O\*NET skills) to a new frame of reference with fewer 
                variables (we preserved 6 principal components) that maintains around 80% of observed
                variance. This means that instead of needing 41 variables to define an occupation, 
                we can instead use 6. This analysis contextualizes the new set of axes we use, since
                these vectors are otherwise a plain enumeration.
                
                Recommended use: sort each principal component to see which skills are polarized.
                """,
                className="text"
            ),
            html.Div(id="data-transformation--pca-loadings-div"),
            dcc.Markdown(
                """
                Based on the coefficients, we suggest the following interpretations for the leading
                4 Principal Components:
                - PC1: Analytical and cognitive skills
                - PC2: Physical skills
                - PC3: Technical and analytical, but penalizing social components
                - PC4: Social-focused skills
                
                While we kept 6 components, the contribution to total variance of PC5 and PC6 
                combined adds up to only 5%, so their interpretation will not be quite as robust.
                """,
                className="text"
            ),
            qa.set_anchor("Crosswalk to NOC", "data-transformation--crosswalk"),
            dcc.Markdown(
                """
                The O\*NET to NOC crosswalk we use is a many-to-many mapping, where occupations 
                from either system can refer to multiple occupations in the other. To generate 
                the NOC dataset, we left join the O\*NET data with the mapping, and then apply 
                an element-wise average for each NOC code. The intermediate dataset has 1216 rows, 
                up from 968. After averaging each group, we have 489 NOC occupations. We present 
                the complete and PCA wide-form O\*NET and NOC datasets.
                """,
                className="text"
            ),
            # pivot data
            html.Div(
                children=[
                    dbc.Button(
                        "O*NET Full",
                        id={
                            "tab": "data-transformation", 
                            "eltype": "btn", 
                            "element": "onet-full"
                        }
                    ),
                    dbc.Button(
                        "O*NET PCA",
                        id={
                            "tab": "data-transformation", 
                            "eltype": "btn", 
                            "element": "onet-pca"
                        }
                    ),
                    dbc.Button(
                        "NOC Full",
                        id={
                            "tab": "data-transformation", 
                            "eltype": "btn", 
                            "element": "noc-full"
                        }
                    ),
                    dbc.Button(
                        "NOC PCA",
                        id={
                            "tab": "data-transformation", 
                            "eltype": "btn", 
                            "element": "noc-pca"
                        }
                    )
                ],
                id="data-transformation--buttons",
                className="row-flex"
            ),
            html.Div(
                children=[
                    html.Div(
                        id="data-transformation--div-onet-full",
                        children=[
                            dash_table.DataTable(
                                data=df_onet_piv.to_dict("records"),
                                columns= [{"name":i, "id":i} for i in df_onet_piv.columns],
                                sort_action="native",
                                filter_action="native",
                                style_cell={
                                    "textAlign": "left",
                                    "whiteSpace": "normal"
                                },
                                style_table={
                                    "overflowX": "auto",
                                    "position": "absolute"
                                },
                                virtualization=True,
                                fixed_rows={"headers":True},
                                style_data_conditional= [
                                    {
                                        "if": {"column_id": cid},
                                        "width": "100px"
                                    } for cid in df_onet_piv.columns
                                ]
                                
                            )
                        ],
                    ),
                    html.Div(
                        id="data-transformation--div-onet-pca",
                        children=[
                            dash_table.DataTable(
                                data= df_onet_pca.to_dict("records"),
                                columns= [{"name":i, "id":i} for i in df_onet_pca.columns],
                                sort_action="native",
                                filter_action="native",
                                style_cell={
                                    "textAlign": "left",
                                    "whiteSpace": "normal"
                                },
                                style_table={
                                    "overflowX": "auto",
                                    "position": "absolute"
                                },
                                virtualization=True,
                                fixed_rows={"headers":True},
                                style_data_conditional= [
                                    {
                                        "if": {"column_id": cid},
                                        "width": "100px"
                                    } for cid in df_onet_pca.columns
                                ]
                                
                            )
                        ],
                    ),
                    html.Div(
                        id="data-transformation--div-noc-full",
                        children=[
                            dash_table.DataTable(
                                data=df_noc_piv.to_dict("records"),
                                columns= [{"name":i, "id":i} for i in df_noc_piv.columns],
                                sort_action="native",
                                filter_action="native",
                                style_cell={
                                    "textAlign": "left",
                                    "whiteSpace": "normal"
                                },
                                style_table={
                                    "overflowX": "auto",
                                    "position": "absolute"
                                },
                                virtualization=True,
                                fixed_rows={"headers":True},
                                style_data_conditional= [
                                    {
                                        "if": {"column_id": cid},
                                        "width": "100px"
                                    } for cid in df_noc_piv.columns
                                ]
                                
                            )
                        ],
                    ),
                    html.Div(
                        id="data-transformation--div-noc-pca",
                        children=[
                            dash_table.DataTable(
                                data= df_noc_pca.to_dict("records"),
                                columns= [{"name":i, "id":i} for i in df_noc_pca.columns],
                                sort_action="native",
                                filter_action="native",
                                style_cell={
                                    "textAlign": "left",
                                    "whiteSpace": "normal"
                                },
                                style_table={
                                    "overflowX": "auto",
                                    "position": "absolute"
                                },
                                virtualization=True,
                                fixed_rows={"headers":True},
                                style_data_conditional= [
                                    {
                                        "if": {"column_id": cid},
                                        "width": "100px"
                                    } for cid in df_noc_pca.columns
                                ]
                            )
                        ],
                    ),
                ],
            ),
            html.Div(children=[html.Br() for i in range(3)])
        ],
    ),
])


@app.callback(
    [
        Output("data-transformation--div-onet-full", "style"),
        Output({"tab": "data-transformation", "eltype": "btn", "element": "onet-full"}, "active"),
        
        Output("data-transformation--div-onet-pca", "style"),
        Output({"tab": "data-transformation", "eltype": "btn", "element": "onet-pca"}, "active"),
        
        Output("data-transformation--div-noc-full", "style"),
        Output({"tab": "data-transformation", "eltype": "btn", "element": "noc-full"}, "active"),
        
        Output("data-transformation--div-noc-pca", "style"),
        Output({"tab": "data-transformation", "eltype": "btn", "element": "noc-pca"}, "active")
    ],
    Input({"tab": "data-transformation", "eltype": "btn", "element": ALL}, "n_clicks")
)
def data_transformation_toggle_table(btn):
    elements = {"onet full", "onet pca", "noc full", "noc pca"}
    btn_to_div = {
        element: {"display": "none"} 
        for element in elements
    }
    
    active = {
        element: False
        for element in elements
    }
    ctx = dash.callback_context
    if not ctx.triggered:
        return (
            {"display": "none"}, False, 
            {"display": "none"}, False, 
            {"display": "none"}, False,
            {"display": "none"}, False
        )
    else:
        last_clicked = ctx.triggered[0]["prop_id"].split(".")[0]
    
    for element in elements:
        # match kw
        if sum(el in last_clicked for el in element.split()) == 2:
            btn_to_div[element] = {"display": "inline"}
            active[element] = True
    
    return (
        btn_to_div["onet full"], active["onet full"], 
        btn_to_div["onet pca"], active["onet pca"], 
        btn_to_div["noc full"], active["noc full"], 
        btn_to_div["noc pca"], active["noc pca"], 
    )


@app.callback(
    Output("data-transformation--pca-subplots", "figure"),
    Input("options--dataset-radio", "value"),
)
def pca_figures(dataset):
    if dataset == "onet":
        data = onet_pca_data
    else:
        data = noc_pca_data
    
    subplot_position_map = {
        "scree": (1,1),
        "cumvar": (2,1),
        "2d": (1,2),
        "3d": (3,1)
    }
    
    data["scree"]["showlegend"] = False
    data["cumvar"]["showlegend"] = False
    
    fig = make_subplots(
        rows=3, cols=2,
        specs = [
            [{}, {"rowspan": 2}],
            [{}, None],
            [{"colspan": 2, "type": "scene"}, None]
        ],
        subplot_titles=("Scree Plot", "2D PCA", "Cumulative Variance", "3D PCA")
    )
    
    for subp, (_row, _col) in subplot_position_map.items():
        fig.append_trace(data[subp], _row, _col)
        
    fig.layout = data["layout"]
    fig.update_layout(height=1500)
    
    return fig


@app.callback(
    Output("data-transformation--pca-loadings-div", "children"),
    Input("options--dataset-radio", "value"),
)
def pca_loadings_table(dataset):
    if dataset == "onet":
        df = onet_loadings
    else:
        df = noc_loadings
        
    styles = discrete_background_color(df) 
    styles.append(
        {
            "if": {"column_id": "Element Name"},
            "textAlign": "left"
        }
    )
    
    columns_settings = [{"name": "Element Name", "id": "Element Name"}]
    columns_settings.extend([
        {
                "name": i, "id": i, 
                "hideable": True,
                "type": "numeric",
                "format" : Format(precision=2, scheme=Scheme.fixed)
            } 
            for i in df.columns[1:]
    ])
    
    table = dash_table.DataTable(
        data = df.to_dict("records"),
        columns = columns_settings,
        
        id= "data-transformation--pca-loadings",
        sort_action="native",
        
        style_data_conditional=styles,
        style_table={
            'overflowX': 'auto',
            'height': '65vh'
        },
        style_cell={
            'height': 'auto',
            # all three widths are needed
            'minWidth': '0px', 'width': '50px', 'maxWidth': '200px',
            'whiteSpace': 'normal'
        },
        css= [{
            "selector": ".show-hide",
            "rule": "padding: 5px 5px"    
        }]
    )
    
    return table
