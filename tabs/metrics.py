#!/usr/bin/env python
import pandas as pd
import json
from plotly.subplots import make_subplots
import plotly.express as px
from scipy.cluster.hierarchy import dendrogram, linkage

from scipy.cluster.hierarchy import ClusterWarning
from warnings import simplefilter
simplefilter("ignore", ClusterWarning)

import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import (
    Output, Input, State
)

from app import app
from utils import QuickAccess, pwise, normalize_df
qa = QuickAccess()

import dash_defer_js_import as dji
mathjax_script = dji.Import(src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_SVG")

layout = html.Div([
    dcc.Markdown(
        """
        We were interested in choosing a function to quantify the notion of distance. 
        The literature widely favors either Euclidean distance, or an alternate formulation
        of cosine similarity that satisfies triangle inequality (L_2-normalized distances).
        
        The primary constraints were:
        
        - The dimensionality of the data
            - What is a neighbor in a 41-dimensional space?
        - The quality of the information
            - Can occupations faithfully be broken down into 4 or 6 components?
        - Should occupational similarities/differences be based on vector magnitudes or angles?
        
        The issue of dimensionality is partly addressed by PCA, where 4 Principal Components (PC) 
        account for 75% of variance, and 6 PC account for 80%. This leads to the next issue, which 
        is about the dataset itself. How much of the job market dynamics can 4 broad families of 
        work activities represent? Would we need to include the other O\*NET indicators?
        
        The final concern is whether we should choose the function based on the magnitude of vectors, 
        or based on their pairwise angle in the chosen space.
        """,
        className="text"
    ),
    qa.set_anchor("Euclidean Distance", "metrics--euclidean"),
    dcc.Markdown(
        """
        Euclidean distance is the most widely used technique, and is well understood given a small enough space. 
        The issue we encounter here is that without PCA, euclidean distances do not preserve the interpretability 
        it has in 3 dimensional space. However, as mentioned throughout this report, PCA successfully preserves 
        80% variance with 6 dimensions, addressing the initial 
        [curse of dimensionality](https://en.wikipedia.org/wiki/Curse_of_dimensionality#Distance_function).
        
        The euclidean distance between the vector representations of occupations $v$ and $w$ is
        $$
        \Vert v - w \Vert = \sqrt{
            \sum^6_{i=1} (v_i - w_i)^2
        }
        $$
        
        The euclidean distance reports a magnitude, as opposed to the cosine distance.
        """,
        className="text"
    ),
    
    qa.set_anchor("Cosine Distance", "metrics--cosine"),
    dcc.Markdown(
        """
        Cosine distance is the complement of cosine similarity: $D_C := 1 - S_C$, where $D_C$ is the 
        cosine distance, and $S_C$ is the cosine similarity. The latter evaluates the angle between two vectors.
        Since every element defining an occupation is positive, then every occupation lies in the positive side 
        of the space. In more familiar spaces, this would mean the 1st quadrant in $\mathbb{R}^2$, and the 1st 
        octant in $\mathbb{R}^3$. Since every vector points in a similar direction, then we have the following guarantee:
        
        $$ \\theta \in \[0, \pi/2\] \implies S_C = cos(\\theta) \in \[0,1\] \implies D_C := 1 - S_C \in \[0,1\]$$
        
        where $cos(\\theta) = \\frac{v \cdot w}{\Vert v \Vert \Vert w \Vert}$
        
        The curse of dimensionality also manifests itself in the original 41-dimensional representation of occupations: 
        since each element is non-negative, then we always discard half of the "added" dimension. For example, in 
        $\mathbb{R}^2$, the 1st quadrant occupies 1/4 of the area. In $\mathbb{R}^3$, the 1st octant occupies $(1/2)^3$ 
        of the volume. By the time we get to $\mathbb{R}^{41}$, our set of occupations will only span at most 
        $(1/2)^{41} \\approx 4.54 \cdot 10^{-13}$. This means that clusters become extremely senstive to small
        changes in pairwise angles.
        
        Another weakness is that coincident vectors are indistinguishable, so a hypothetical occupation $v' := kv$ will 
        be seen as identical to occupation $v$ when it is not the case.
        """,
        className="text"
    ),
    qa.set_anchor("Mahalanobis Distance", "metrics--mahalanobis"),
    # https://analyticalsciencejournals.onlinelibrary.wiley.com/doi/full/10.1002/cem.2692  ## maha <-> PCA
    dcc.Markdown(
        """
        The third measure of similarity under consideration is the Mahalanobis distance. Unlike the previous two, 
        Mahalanobis distance measures the distance between a point and a distribution. It generalizes the Euclidean
        distance and addresses a few of its limiting assumptions. 
        
        Whereas Euclidean distance is sensitive to scale and assumes that every dimension is equally weighted 
        (i.e. orthogonal dimensions, which is not the case as shown by PCA), Mahalanobis considers the data 
        holistically as opposed to pairwise. 
        
        The function is similar to a Z-score, where data is first centered, and then standardized. In the same vein, both scores
        are scale-invariant and unitless, as they represent distance from a distribution mean, measured in standard deviations.
        Mahalanobis distance, however, is not restricted to the univariate assumption of the Z-score and Euclidean distance approach.
        
        By constructing a covariance matrix $S$, we account for interactions between different skills, which allows us to 
        deal with [non-spherical distributions](https://en.wikipedia.org/wiki/Mahalanobis_distance#Intuitive_explanation).
        
        Let $x$ be the 41-dimensional vector representation of an occupation, and $\\mu$ the vector of means for each skill, 
        and $S$ the covariance matrix. The Mahalanobis distance of occupation $x$ is defined as
        
        $$
            D_M(x) = \sqrt{(x - \\mu)^\\top S^{-1} (x - \\mu)}
        $$
        given the same cluster, Mahalanobis can also be applied pairwise by substituting $\\mu$ with another occupation $x_2$.
        
        As a generalization, the Mahalanobis distance becomes the same as Euclidean distance when $S = I_n$
        """,
        className="text"
    ),
    mathjax_script
])
