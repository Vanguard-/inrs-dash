#!/usr/bin/env python
import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import (
    Output, Input, State
)

import json
import pandas as pd
import numpy as np

from app import app
from utils import QuickAccess, normalize_df
from neighbor_utils import apply_marginal, lines, violins

qa = QuickAccess()

import dash_defer_js_import as dji
mathjax_script = dji.Import(src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_SVG")

maps = {}

with open("data/index/onet.json") as f:
    maps["onet"] = json.load(f)
with open("data/index/noc.json") as f:
    _noc = json.load(f)
    _noc = {int(float(k)): v for k, v in _noc.items()}
    maps["noc"] = _noc

dfs = {
    dataset: {
        method: normalize_df(
            pd.read_csv(
                f"data/neighbor/{dataset}_pca_{method}.csv",
                index_col=index_column
            )
        ) for method in {"euclidean", "mahalanobis", "cosine"}
    } for dataset, index_column in zip(["onet", "noc"], ["O*NET-SOC Code", "NOC"])
}

layout = html.Div([
    dcc.Store(id="neighbor--onet-paths", data={}),
    dcc.Store(id="neighbor--noc-paths", data={}),
    dcc.Store(id="neighbor--apply-marginal"),
    dcc.Markdown(
        """        
        Another approach is to look at neighborhoods of an occupation. This section analyzes the distribution of
        pairwise distances to form a data-driven threshold.

        In the previous section, we normalized the pairwise distances. We believe that this creates pivotal quantities,
        and allows us to make decisions on the distribution as a whole. In fact, pre-normalization, the distances
        obtained were not comparable: euclidean distances must be interpreted in the context of its $n$-dimentional euclidean space,
        mahalanobis distance with respect to the data's standard deviation, and cosine distance is a conversion from 
        angles. In normalizing the data, we remove each occupation's idiosyncratic range, and the interpretation shifts from some
        context-based number to a proportion of an occupation's range.
        
        Choosing cluster sizes means choosing a threshold for pairwise distances. For this exercise,
        we use the following notation: for an occupation $v$, and a  threshold $t_v$, $F_v(t_v)$ is the number of 
        occupations chosen to be "in the same cluster as occupation $v$". 
        
        Whereas in the previous section we wanted to find a theoretical "representative occupation" to act as the center of a cluster,
        this section assumes that each occupation is its own center. The reason is that clustering algorithms usually focus on 
        intra- connected component interactions, enforcing a kind of polarization regarding membership. The reality we want to 
        represent is, however, less restrictive. It is sensible for some occupations to lie on the boundary of its cluster, acting
        like a bridge between industries. The idea is then to determine a neighborhood around each point, and then gauge their overlaps.
        
        Our objective is then to choose $t_v$ for each $v$ such that for a large $\\epsilon$, $\\left\(F_v(t_v + \\epsilon) - F_v(t_v) \\right\) = \\delta$ 
        is small. Specifically, a large $\\epsilon$ for a small $\\delta$ means that we would need to travel to 
        further nodes in the network, suggesting that we are including an occupation belonging to a different cluster into our search range.
        
        The results found here could also be used to justify hyperparameters for clustering methods such as DBSCAN.
        """,
        className="text"
    ),
    qa.set_anchor("Methodology", "neighbor--methodology"),
    dcc.Markdown(
        """
        Since we observe the cumulative density $F_v(t_v)$ for all $t_v$, we can choose a specific reference point $R = F_v(t_v)$ 
        and gap $\\delta$ to estimate $\\epsilon$. By constructing a sequence { $R, R + \\delta$ }$^n\\_{p=0}$, we can compute a sequence 
        of $\\epsilon_p$ from $\[R, R+ \\delta\] = \[F_v(t_v), F_v(t_v + \\epsilon)\]$, representing the derivative 
        of $F_v(t_v)$ with respect to $t_v = \sum \\epsilon_p$.
        
        We will construct the sequence by choosing $L$, the distance between each reference point, to partition the domain.
        Note that the results are ill defined near the tail, when $R + \\delta > 1$. 
        
        The graph below works for both O\*NET and NOC occupations, and all three distance functions. Moreover, you can plot the path of 
        specific occupations. **The paths require visibility of all traces.** For O\*NET, color is based on the major group. For NOC, color changes
        every 1000.
        """,
        className="text"
    ),
    qa.set_anchor("Output", "neighbor--output"),
    html.Div(
        id="neighbor--output-container",
        children = [
            html.Div(
                id="neighbor--paths",
                children=[
                    dcc.Dropdown(
                        id="neighbor--paths-ddown", 
                        className="text", 
                        multi=True,
                        placeholder="Select paths"
                    )
                ]
            ),
            html.Div(
                id="neighbor--ldelta",
                children= [
                    html.Div(
                        children=[
                            html.Span("L = "),
                            dcc.Input(
                                id="neighbor--l", 
                                type="number", 
                                min=0, max=100,
                                value=1,
                                size="3",
                                debounce=True
                            )
                        ]
                    ),
                    html.Div(
                        children=[
                            html.Span("$\\delta$ = "),
                            dcc.Input(
                                id="neighbor--delta", 
                                type="number", 
                                min=0, max=100,
                                value=5,
                                size="3",
                                debounce=True
                            )
                        ]
                    ),
                ],
                className="container row-flex gap text"
            ),
            dcc.Loading(
                dcc.Graph(id="neighbor--centipede"),
                parent_className='loading_wrapper'
            ),
            
        ]
    ),
    mathjax_script
])


# Setups
# path options
@app.callback(
    Output("neighbor--paths-ddown", "options"),
    Output("neighbor--paths-ddown", "value"),
    Input("options--dataset-radio", "value")
)
def update_paths_options(dataset):
    options = [
        {
            "label": f"{k} {v}",
            "value": k
        } for k, v in maps[dataset].items()
    ]
    return options, None


"""
# apply_marginal df
@app.callback(
    [
        Output("neighbor--apply-marginal", "data"),
        Output("neighbor--onet-paths", "clear_data"),
        Output("neighbor--noc-paths", "clear_data"),
    ],
    Input("neighbor--confirm-btn", "n_clicks"),
    [
        State("neighbor--l", "value"),
        State("neighbor--delta", "value"),
        State("options--dataset-radio", "value"),
        State("options--method-radio", "value"),
    ]
)
def apply_marginal_df(nclicks, l, delta, dataset, method):
    generate = [
        (a, delta) for a in np.arange(1, 101, l) 
    ]
    
    d = apply_marginal(
        dfs[dataset][method], 
        generate
    )
    res = d.reset_index().T.reset_index().T.to_dict("records")
    return res, True, True

"""
@app.callback(
    Output("neighbor--apply-marginal", "data"),
    Output("neighbor--onet-paths", "clear_data"),
    Output("neighbor--noc-paths", "clear_data"),
    Input("neighbor--l", "value"),
    Input("neighbor--delta", "value"),
    Input("options--dataset-radio", "value"),
    Input("options--method-radio", "value")
)
def apply_marginal_df(l, delta, dataset, method):
    generate = [
        (a, delta) for a in np.arange(1, 101, l) 
    ]
    
    d = apply_marginal(
        dfs[dataset][method], 
        generate
    )
    
    res = d.reset_index().T.reset_index().T.to_dict("records")
    return res, True, True
"""

"""
@app.callback(
    Output("neighbor--onet-paths", "data"),
    Output("neighbor--noc-paths", "data"),
    Input("neighbor--paths-ddown", "value"),
    Input("neighbor--apply-marginal", "data"),
    State("neighbor--onet-paths", "data"),
    State("neighbor--noc-paths", "data"),
    State("options--dataset-radio","value"),
    prevent_initial_call=True
)
def update_paths(paths, d_dict, onet_paths, noc_paths, dataset):
    if paths is None:
        return {}, {}
        
    d = pd.DataFrame(d_dict)
    d.columns = d.iloc[0]
    d.index = d.iloc[:, 0]
    d = d.iloc[1:, 1:]
    d.index.name = ""
    d.columns = list(map(tuple, d.columns))

    
    if dataset == "onet":
        if onet_paths is None: 
            onet_paths = {}
        for path in paths:
            if path in onet_paths:
                continue
            onet_paths[path] = lines(d, path)
        
        return onet_paths, {}
        
    else:
        if noc_paths is None: 
            noc_paths = {}
        for path in paths:
            if path in noc_paths:
                continue
            noc_paths[path] = lines(d, path)
            
        return {}, noc_paths
    
    
"""
@app.callback(
    Output("neighbor--onet-paths", "data"),
    Input("neighbor--paths-ddown", "value"),
    State("options--dataset-radio","value"),
    State("neighbor--onet-paths", "data"),
    State("neighbor--apply-marginal", "data"),
    prevent_initial_call=True
)
def update_onet_paths(paths, dataset, data, d_dict):
    
    if dataset == "noc":
        return {} # clear cache
    
    if paths is None:
        return {}
    
    d = pd.DataFrame(d_dict)
    d.columns = d.iloc[0]
    d.index = d.iloc[:, 0]
    d = d.iloc[1:, 1:]
    d.index.name = ""
    d.columns = list(map(tuple, d.columns))
    
    if data is None:
        data = {}
        
    for path in paths: 
        if path in data: 
            continue
        
        data[path] = lines(d, path)
    return data


@app.callback(
    Output("neighbor--noc-paths", "data"),
    Input("neighbor--paths-ddown", "value"),
    State("options--dataset-radio","value"),
    State("neighbor--noc-paths", "data"),
    State("neighbor--apply-marginal", "data"),
    prevent_initial_call=True
)
def update_noc_paths(paths, dataset, data, d_dict):
    if dataset == "onet":
        return {} # clear cache
    
    if paths is None:
        return {}
    
    d = pd.DataFrame(d_dict)
    d.columns = d.iloc[0]
    d.index = d.iloc[:, 0]
    d = d.iloc[1:, 1:]
    d.index.name = ""
    d.columns = list(map(tuple, d.columns))
    
    if data is None:
        data = {}
    
    for path in paths: 
        if path in data: 
            continue
        
        data[path] = lines(d, path)
    
    return data
"""    


@app.callback(
    Output("neighbor--centipede", "figure"),
    Input("neighbor--onet-paths", "data"),
    Input("neighbor--noc-paths", "data"),
    Input("neighbor--apply-marginal", "data"),
    State("options--dataset-radio","value"),
    State("neighbor--paths-ddown", "value")
)
def update_centipede(onet_paths, noc_paths, d_dict, dataset, selected_paths):
    d = pd.DataFrame(d_dict)
    d.columns = d.iloc[0]
    d.index = d.iloc[:, 0]
    d = d.iloc[1:, 1:]
    d.index.name = ""
  
    d.columns = list(map(tuple, d.columns))
    
    
    fig = violins(d)
    
    if selected_paths:
        selected_paths = [str(i) for i in selected_paths]
        paths = noc_paths if dataset == "noc" else onet_paths

        subset = {k:v for k, v in paths.items() if k in selected_paths}
        shapes = sum(subset.values(), [])
        fig.layout["shapes"] = shapes
    
    return fig
    
    
