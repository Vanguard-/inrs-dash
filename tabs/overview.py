#!/usr/bin/env python
import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import (
    Output, Input, State
)
import dash

from app import app
from utils import QuickAccess
qa = QuickAccess()

layout = html.Div([
    dcc.Markdown(
        """
        The objective of this project is to find a way to measure occupational similarity. Current standards 
        are established by the [O\*NET database](https://www.onetcenter.org/overview.html), sponsored by the 
        US Department of Labor, and relies on the inputs of professionals and domain experts. The end goal is 
        the construction of an occupational mobility model that can reflect the realities of the
        labor market. 
        
        This is an **exploratory step** in the project. The current analysis only uses the **Work Activities**
        determinants of an occupation. The archive includes other fields, such as general quantifiers, work 
        contexts, and training expectations. Moreover, occupational similarity (or distance) enforces a symmetry 
        that the true transition processes may not have, such as when an occupation requires more education, 
        experience, or training than another. 
        
        This report presents our methodology and analysis for occupation clustering. Since our focus was to explore 
        the potential of this point of entry, we were satisfied with using only a subset of the available fields.
        
        Our starting point is the data from the US Occupation Information Network (O\*NET).
        It provides a list of occupations, each differentiated by vectors of work activities defined by domain experts.
        Each skill is evaluated based on its importance and level. From O\*NET, we can apply a crosswalk for relevance in the 
        Canadian labor market, going from O\*NET codes to Canadian National Occupational Classification (NOC) codes.
        
        This project assumes that the American and Canadian labor markets can be properly modelled as diffusion
        processes, where nodes are occupations, and edges represent transitions between occupations. Moreover, we 
        assume that connected components exist in the graph. The existence of a graph structure motivates our search
        for occupational similarity --- a first step towards modelling the transition dynamics.
        The current goal is to robustly identify clusters of occupations.
        
        Our crosswalk limits us to version 25.0 of the O\*NET dataset.
        """,
        className="text"
    ),
    qa.set_anchor("Technical Note", "technical-note"),
    dcc.Markdown(
        """
        **Graph Interactivity**: Graphs are responsive in many ways. **Hovering** over an element displays a tooltip;
        **box select**, and **vertical** or **horizontal** selections focuses the chosen region. **Double click** resets
        the zoom. 
        Elements in the legend can be shown or hidden with a simple click. **Double click** focuses a specific trace,
        and likewise can undo selections. The configurations bar on the top right of the graphs offer a more explicit interface.
        
        
        **Sidebar**: Given that this exploratory step focuses on two labor markets and the application of distance metrics,
        the options in the sidebar will update relevant graphs whenever possible, throughout the report.
        
        **Focus**: The components surrounding the core writeup can be distracting and take too much space; the "Focus" button
        will reduce the size of these extraneous components. In situations where graphs have expanded, the button
        may not work as intended and fail to recover the expected state. Changing tabs or refreshing the page will
        recover this. 
        
        **App Memory**: Certain pages display computationally demanding tasks. The amount of memory allocated to this
        page is limited. The tab name will display "Updating..." whenever a graph is pending update.
        """,
        className="text"
    )
])


