import plotly.express as px
import plotly.graph_objects as go

import pandas as pd
import numpy as np
from scipy.stats import gaussian_kde as kde
from plotly.colors import n_colors

noc_colors = dict(zip(
    range(1_000, 11_000, 1_000), 
    px.colors.qualitative.Light24
))

onet_colors = dict(zip(
    range(11,55,2), 
    px.colors.qualitative.Light24
))

def set_color(path):
    if isinstance(path, str): #is onet
        return onet_colors[int(path[:2])]
    
    for upper_bound in range(1_000,11_000, 1_000):
        if path < upper_bound:
            return noc_colors[upper_bound]
    

# Neighbor specific helpers
def marginal(arr, start=10, steps=1):
    gap = np.diff(
        np.percentile(
            arr, [start, start+steps]
        )
    )[0]
    return gap

def apply_marginal(df, generate):
    d = pd.DataFrame()
    for a, b in generate:
        try:
            d[(a,b)] = df.apply(
                lambda x: marginal(x, a,b)
            )
        except:
            break
    return d

def get_index(data, val):
    pdf = kde(list(data))

    vals_min = np.min(data)
    vals_max = np.max(data)
    xx = np.linspace(vals_min, vals_max, 100)

    yy = pdf(xx)

    maxyy = np.max(yy)
    minyy = np.min(yy)

    nyy = (yy - minyy)/(maxyy-minyy)

    index = (np.abs(xx - val)).argmin()
    return index, xx, nyy

def lines(d, path):
    if path not in d.index:
        return
    
    fig = go.Figure()
    
    color = set_color(path)
    print(color)
    sub = d.loc[path]
    for offset, col in enumerate(d.columns):
        data = d[col].to_numpy()
        index, xx, nyy = get_index(data, sub[col])
        x_ = xx[index]
        y_ = nyy[index]
        fig.add_shape(x0=x_, x1= x_, y0=offset, y1 = 1.5 * y_ + offset, line_color=color)
    
    
    return fig.layout["shapes"]


def violins(d):
    fig = go.Figure()
    colors = n_colors('rgb(5, 100, 100)', 'rgb(5, 200, 200)', len(d.columns), colortype='rgb')
    for offset, (col, color) in enumerate(zip(d.columns, colors)):
        start, step = col  # unpack

        data = d[col].to_numpy()
        fig.add_trace(
            go.Violin(
                x = data, line_color=color,
                name=str(col)
            )
        )
        
    fig.update_traces(orientation='h', side='positive', width=3, points=False)
    fig.update_layout(
        xaxis_showgrid=False, xaxis_zeroline=False, 
        height=700
    )
    
    return fig