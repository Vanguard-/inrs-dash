import pandas as pd
import os
import importlib

import dash 
from utils import QuickAccess


# https://stackoverflow.com/questions/59903698/multi-page-dash-app-callbacks-not-registering
app = dash.Dash(__name__)
app.config.suppress_callback_exceptions = True # not sure how to handle validation layout
server = app.server

files = [i.split(".py")[0] for i in os.listdir("tabs") if i.endswith(".py")]
layouts_dict = {f"{file}": importlib.import_module(f"tabs.{file}").layout for file in files}
qa_dict = {f"{file}": importlib.import_module(f"tabs.{file}").qa for file in files}

app.index_string = '''
<!DOCTYPE html>
<html>
    <head>
        {%metas%}
        <title>{%title%}</title>
        {%favicon%}
        {%css%}
    </head>
    <body>
        {%app_entry%}
        <footer>
            {%config%}
            {%scripts%}
            <script type="text/x-mathjax-config">
            MathJax.Hub.Config({
                tex2jax: {
                inlineMath: [ ['$','$'],],
                processEscapes: true
                }
            });
            </script>
            {%renderer%}
        </footer>
    </body>
</html>
'''